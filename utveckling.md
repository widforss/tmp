Scouterna bedriver idag ett utvecklingsarbete som alla Sveriges scouter kan ta del av. Under den nyligen avslutade omorganisationen har det varit nödvändigt att ha ett omfattande utvecklingsarbete för att den nya organisationen skulle hitta sitt arbetssätt och identitet. Vi tror att man idag inte behöver en lika omfattande utvecklingsorganisation och att man kan genomföra ett större arbete på ideell basis.

Ansgarsförsamlingens Ungdom i Västerås föreslår beredningskonferensen:

* att Equmenia arbetar för att Scouterna minskar omfattningen av sin utvecklingsorganisation.
* att Equmenia arbetar för att Scouterna förändrar sin medlemsavgift i proportion till eventuella förändringar i Scouternas utvecklingsorganisations omfattning.
