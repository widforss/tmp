Equmenia kan i framtiden få en högre avgift för sina scouters medlemskap i Scouterna. Det kan tyckas orättvist då Equmenias scouter inte tar del av den gemensamma verksamheten i samma grad som andra scouter. Vi i Ansgarsförsamlingens Ungdom i Västerås upplever att en stor del av orsaken till det ojämna deltagandet är att lokala equmeniascouter inte får ta del av mycket av den info som Scouterna har att ge. Om Scouterna fick möjlighet att kommunicera direkt med kårer, och kanske till och med medlemmar skulle återbäringen av medlemsavgiften bli större för Equmenias scouter.

Ansgarsförsamlingens Ungdom i Västerås föreslår beredningskonferensen:

* att Equmenia arbetar för att tillåta direkt kommunikation mellan Scouterna och Equmenias scoutkårer.
* att Equmenia arbetar för att tillåta direkt kommunikation mellan Scouterna och Equmenias scouter.
