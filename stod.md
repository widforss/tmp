Idag betalar Equmenias scouter 180 kr i medlemsavgift till Scouterna. Därefter får Equmenia ett stöd från Scouterna som uppgår till en summa som gör att nettokostnaden för Equmenia blir 60 kr per scout. Detta är en tredjedel av kostnaden för NSF och SALT, något som Scouterna upplever orättvist. En mer rättvis lösning vore att använda stödet för att sänka medlemsavgiften för samtliga av Sveriges scouter.

Ansgarsförsamlingens Ungdom i Västerås föreslår beredningskonferensen:

* att Equmenia arbetar för att Scouterna sänker medlemsavgiften med en summa motsvarande hela Equmenias stöd uppdelat på Sveriges alla scouter.
